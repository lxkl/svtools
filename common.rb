SCAN_DIR = ENV["SVTOOLS_SCAN_DIR"] || "/var/service"
SERVICE_REPOS = ENV["SVTOOLS_SERVICE_REPOS"] || "/etc/sv"
CONFIG = ENV["SVTOOLS_CONFIG"] || "#{SERVICE_REPOS}/config.yaml"

def complete_svdir(x)
  return x if x.start_with?("/") || x.start_with?("./")
  "#{SCAN_DIR}/#{x}"
end
